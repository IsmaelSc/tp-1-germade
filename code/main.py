import requests

lat = "37.8267"
lon = "-122.4233"

url = f"https://api.darksky.net/forecast/955c5b3e086941a36d2c9a13c0b75a8f/{lat},{lon}"

response = requests.get(url)

print(response.status_code)

guardar = open("result.txt", "w")

guardar.write(str(response.text))
guardar.write(str(response.status_code))
guardar.write(str(response.headers["Content-Encoding"]))
guardar.write(str(response.headers["Content-Type"]))